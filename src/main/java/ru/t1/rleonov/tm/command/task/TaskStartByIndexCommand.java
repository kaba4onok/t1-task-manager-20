package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public final static String COMMAND = "task-start-by-index";

    public final static String DESCRIPTION = "Start task by index.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
