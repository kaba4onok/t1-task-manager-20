package ru.t1.rleonov.tm.exception.field;

import ru.t1.rleonov.tm.exception.user.AbstractUserException;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
