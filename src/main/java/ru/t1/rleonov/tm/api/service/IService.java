package ru.t1.rleonov.tm.api.service;

import ru.t1.rleonov.tm.api.repository.IRepository;
import ru.t1.rleonov.tm.enumerated.Sort;
import java.util.List;

public interface IService<M> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
