package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, String name, String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
            if (!task.getUserId().equals(userId)) continue;
            projectTasks.add(task);
        }
        return projectTasks;
    }

}
